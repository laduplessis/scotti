/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multitypetreeVolz.util;

import beast.core.CalculationNode;
import beast.core.Description;
import beast.core.Input;
import beast.core.Function;
import beast.core.Input.Validate;
import beast.core.Loggable;
//import beast.evolution.tree.MultiTypeNodeConcise;
//import beast.evolution.tree.MultiTypeNodeVolz;
import beast.evolution.tree.MultiTypeTreeConcise;
//import beast.evolution.tree.MultiTypeTreeVolz;




//import beast.evolution.tree.Node;
//import beast.evolution.tree.Tree;

import beast.evolution.tree.Node;

import java.io.PrintStream;

import multitypetreeVolz.distributions.StructuredCoalescentTreeDensityNew;

@Description("Logger to report root type of a multi-type tree.")
public class TreeLoggerConcise extends CalculationNode implements Loggable, Function {

    public Input<StructuredCoalescentTreeDensityNew> sCInput = new Input<StructuredCoalescentTreeDensityNew>(
            "StructuredCoalescentTreeDensityConcise", "StructuredCoalescentTreeDensityConcise to extract tree from.", Validate.REQUIRED);
    
    StructuredCoalescentTreeDensityNew treeDensity;
    MultiTypeTreeConcise mtTree;
    Integer[] countList;
    String[] demeList;
    
    @Override
    public void initAndValidate() {
    	treeDensity=sCInput.get();
    	//treeDensity.setAllDemes();
        mtTree = treeDensity.getTree();
        countList=treeDensity.countList;
        demeList=treeDensity.demeList;
    }

    @Override
    public void init(PrintStream out) throws IllegalArgumentException {
        if (getID() == null || getID().matches("\\s*")) {
            out.print(mtTree.getID() + ".annotatedTree\t");
        } else {
            out.print(getID() + "\t");
        }
    }

    @Override
    public void log(int nSample, PrintStream out) {
        out.print(" = "+toSortedNewickConcise(mtTree.getRoot(), new int[1],demeList,countList)+";");
    }
    
    public String toSortedNewickConcise(Node node, int[] iMaxNodeInClade, String[] demeList, Integer[] countList) {
        StringBuilder buf = new StringBuilder();
        if (node.getLeft() != null) {
            buf.append("(");
            String sChild1 = toSortedNewickConcise(node.getLeft(), iMaxNodeInClade, demeList, countList);
            int iChild1 = iMaxNodeInClade[0];
            if (node.getRight() != null) {
                String sChild2 = toSortedNewickConcise(node.getLeft(), iMaxNodeInClade, demeList, countList);
                int iChild2 = iMaxNodeInClade[0];
                if (iChild1 > iChild2) {
                    buf.append(sChild2);
                    buf.append(",");
                    buf.append(sChild1);
                } else {
                    buf.append(sChild1);
                    buf.append(",");
                    buf.append(sChild2);
                    iMaxNodeInClade[0] = iChild1;
                }
            } else {
                buf.append(sChild1);
            }
            buf.append(")");
            if (getID() != null) {
                buf.append(node.getNr()+1);
            }
        } else {
            iMaxNodeInClade[0] = node.getNr();
            buf.append(node.getNr() + 1);
        }

        //if (printMetaData) {
        buf.append(("[&host="+demeList[node.getNr()]+",numTransmissions="+countList[node.getNr()]+"]"));
        //}
        buf.append(":").append(node.getLength());
        return buf.toString();
    }   

    @Override
    public void close(PrintStream out) { };

    @Override
    public int getDimension() {
        return 1;
    }

    @Override
    public double getArrayValue() {
        return countList[0];
    }

    @Override
    public double getArrayValue(int iDim) {
        return countList[iDim];
    }
}

