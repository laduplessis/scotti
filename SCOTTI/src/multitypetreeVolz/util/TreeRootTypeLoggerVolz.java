/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multitypetreeVolz.util;

import beast.core.CalculationNode;
import beast.core.Description;
import beast.core.Input;
import beast.core.Function;
import beast.core.Input.Validate;
import beast.core.Loggable;
import beast.evolution.tree.MultiTypeNodeVolz;
import beast.evolution.tree.MultiTypeTreeVolz;

import java.io.PrintStream;

@Description("Logger to report root type of a multi-type tree.")
public class TreeRootTypeLoggerVolz extends CalculationNode implements Loggable, Function {

    public Input<MultiTypeTreeVolz> multiTypeTreeInput = new Input<MultiTypeTreeVolz>(
            "multiTypeTreeVolz", "MultiTypeTreeVolz to report root type of.", Validate.REQUIRED);

    MultiTypeTreeVolz mtTree;
    
    @Override
    public void initAndValidate() {
        mtTree = multiTypeTreeInput.get();
    }

    @Override
    public void init(PrintStream out) throws IllegalArgumentException {
        if (getID() == null || getID().matches("\\s*")) {
            out.print(mtTree.getID() + ".rootColor\t");
        } else {
            out.print(getID() + "\t");
        }
    }

    @Override
    public void log(int nSample, PrintStream out) {
        out.print(((MultiTypeNodeVolz)mtTree.getRoot()).getNodeType() + "\t");
    }

    @Override
    public void close(PrintStream out) { };

    @Override
    public int getDimension() {
        return 1;
    }

    @Override
    public double getArrayValue() {
        return ((MultiTypeNodeVolz)mtTree.getRoot()).getNodeType();
    }

    @Override
    public double getArrayValue(int iDim) {
        return ((MultiTypeNodeVolz)mtTree.getRoot()).getNodeType();
    }
}

