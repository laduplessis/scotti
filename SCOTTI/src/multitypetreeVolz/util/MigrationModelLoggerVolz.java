/*
 * Copyright (C) 2015 Tim Vaughan (tgvaughan@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multitypetreeVolz.util;

import beast.core.BEASTObject;
import beast.core.Input;
import beast.core.Input.Validate;
import beast.core.Loggable;
import beast.evolution.tree.MigrationModelUniform;
import beast.evolution.tree.MultiTypeTreeVolz;
import java.io.PrintStream;

/**
 *
 * @author Tim Vaughan (tgvaughan@gmail.com)
 */
public class MigrationModelLoggerVolz extends BEASTObject implements Loggable {

    public Input<MigrationModelUniform> migModelInput = new Input<>("migrationModel",
        "Migration model to log.", Validate.REQUIRED);

    public Input<MultiTypeTreeVolz> multiTypeTreeInput = new Input<>(
        "multiTypeTree", "Tree from which to acquire type names.");

    private MigrationModelUniform migModel;
    private MultiTypeTreeVolz mtTree;

    @Override
    public void initAndValidate() throws IllegalArgumentException {
        migModel = migModelInput.get();
        mtTree = multiTypeTreeInput.get();
    }

    @Override
    public void init(PrintStream out) throws IllegalArgumentException {
        String outName;
        if (migModel.getID() == null || migModel.getID().matches("\\s*"))
            outName = "migModel";
        else
            outName = migModel.getID();
        
        out.print(outName + ".rate" + "\t");
        out.print(outName + ".nDemes" + "\t");

        for (int i=0; i<migModel.getNumDemes(); i++) {
            if (mtTree != null)
                out.print(outName + ".nDemes" + "\t");
            else
                out.print(outName + ".popSize_" + i + "\t");
        }
        
    }

    @Override
    public void log(int nSample, PrintStream out) {
    	out.format("%g\t", migModel.getRate());
    	out.format("%g\t", migModel.getNumDemes());
        
    }

    @Override
    public void close(PrintStream out) {
    }
    
}
