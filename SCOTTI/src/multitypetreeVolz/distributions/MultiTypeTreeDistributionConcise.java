/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multitypetreeVolz.distributions;

import beast.core.*;
import beast.core.Input.Validate;
import beast.evolution.tree.MultiTypeTreeConcise;

import java.util.List;
import java.util.Random;

/**
 *
 * @author Nicola De Maio
 */
public class MultiTypeTreeDistributionConcise extends Distribution {

	public Input<MultiTypeTreeConcise> mtTreeInput = new Input<MultiTypeTreeConcise>("multiTypeTreeConcise",
			"Volz-type tree.", Validate.REQUIRED);


	// Interface requirements:

	@Override
	public List<String> getArguments() {
		return null;
	}

	@Override
	public List<String> getConditions() {
		return null;
	}

	@Override
	public void sample(State state, Random random) {
	}
	
}
