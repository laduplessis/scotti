/*
 * Copyright (C) 2015 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.evolution.tree;


import beast.core.Description;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Nicola De Maio
 */
@Description("A concise representation of integer vectors when most of the elements are 0 (useful in the manyDemes scenario).")
public class conciseVectorInt {
	
	public int numDemes;
	/**value shared by most entries**/
	//public double commonValue;
	/**list of populations that have uncommon value**/
	public List<Integer> diffIndeces;
	/**List of unusual values**/
	public List<Integer> diffValues;
	
	/**Create inital concise vector without anything inside**/
    public conciseVectorInt(int nD) {
		numDemes=nD;
		//commonValue=0.0;
		diffIndeces = new ArrayList<Integer>();
		diffValues = new ArrayList<Integer>();
	}

    /**join two ordered lists of indeces**/
    public List<Integer> joinIndeces(List<Integer> l1, List<Integer> l2){
    	List<Integer> l3 = new ArrayList<Integer>();
    	int i1=0;
    	int i2=0;
    	int e1=l1.get(i1);
    	int e2=l2.get(i2);
    	int s1=l1.size();
    	int s2=l2.size();
    	while (i1< s1 && i2< s2 ){
    		if (e1<e2){
    			l3.add(e1);
    			i1++;
    			if (i1<s1) e1=l1.get(i1);
    		}else if (e2<e1){
    			l3.add(e2);
    			i2++;
    			if (i2<s2) e2=l2.get(i2);
    		}else{
    			l3.add(e2);
    			i2++;
    			i1++;
    			if (i2<s2) e2=l2.get(i2);
    			if (i1<s1) e1=l1.get(i1);
    		}
    	}
    	for (;i1<s1;i1++) l3.add(l1.get(i1));
    	for (;i2<s2;i2++) l3.add(l2.get(i2));
    	
    	return l3;
    }
    
    /**find sum of all entries in conciseVector**/
    public int sumV(){
    	int sum=0;
    	//sum+=commonValue*(numDemes - diffIndeces.size());
    	for (int i=0;i<diffValues.size();i++){
    		sum+=diffValues.get(i);
    	}
    	return sum;
    }
    
    /**add some value to one of the entries**/
    public void add(int deme, int value){
    	boolean found=false;
    	int i=0;
    	for (;i<diffIndeces.size();i++){
    		if (diffIndeces.get(i)>deme)
    			break;
    		if (diffIndeces.get(i)==deme) {
    			found=true;
    			if (diffValues.get(i)+value==0){
    				diffValues.remove(i);
    				diffIndeces.remove(i);
    			}else diffValues.set(i, diffValues.get(i)+value);
    			break;
    		}
    	}
    	if (!found) {
    		diffIndeces.add(i,deme);
    		diffValues.add(i,value);
    	}
    }
    
    //debugging
    public static void main(String[] argv) throws Exception {
    	conciseVectorInt v1 = new conciseVectorInt(10);
    	v1.add(1, 1);
    	v1.add(2, 2);
    	v1.add(3, 3);
    	v1.add(1, -1);
    	v1.add(4, -1);
    	System.out.println("Sum: "+v1.sumV());
    	System.out.println("Actual result: "+v1.numDemes+" "+v1.diffIndeces+" "+v1.diffValues+" ");
    	
    }


}
