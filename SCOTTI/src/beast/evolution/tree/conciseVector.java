/*
 * Copyright (C) 2015 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.evolution.tree;


import beast.core.Description;
import beast.util.Randomizer;
//import multitypetreeVolz.distributions.StructuredCoalescentTreeDensityConcise;



import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Nicola De Maio
 */
@Description("A concise representation of vectors when most of the elements are the same and only few are different (useful in the manyDemes scenario).")
public class conciseVector {
	
	public int numDemes;
	/**value shared by most entries**/
	public double commonValue;
	/**list of populations that have uncommon value**/
	public List<Integer> diffIndeces;
	/**List of unusual values**/
	public List<Double> diffValues;
	public List<Integer> inactiveDemes;
	
	
	/** creates starting concise vector for a deme just sampled **/
	public conciseVector(int nD, int d, List<Integer> iD) {
		if (d>=nD) {
			System.out.println("ConciseVector creation error: deme specified larger than number of demes");
			System.exit(0);
		}
		numDemes=nD;
		commonValue=0.0;
		diffIndeces = new ArrayList<Integer>();
		diffValues = new ArrayList<Double>();
		diffIndeces.add(d);
		diffValues.add(1.0);
		if (iD!=null) inactiveDemes=new ArrayList<Integer>(iD);
		else inactiveDemes=new ArrayList<Integer>();
		if (inactiveDemes.contains(d)) {
			System.out.println("Cannot create concise vector, specified deme "+d+" is inactive");
			System.exit(0);
		}
	}
    
	/** creates copy of concise vector for a conciseVector **/
	public conciseVector copy() {
		conciseVector cV= new conciseVector(numDemes, inactiveDemes);
		if (diffIndeces!=null)	cV.diffIndeces = new ArrayList<Integer>(diffIndeces);
		else cV.diffIndeces = new ArrayList<Integer>();
		
		if (diffValues!=null) cV.diffValues = new ArrayList<Double>(diffValues);
		else cV.diffValues = new ArrayList<Double>();
		
		if (inactiveDemes!=null) cV.inactiveDemes = new ArrayList<Integer>(inactiveDemes);
		else cV.inactiveDemes = new ArrayList<Integer>();
		
		cV.commonValue = commonValue;
		return cV;
	}
	
	/**Create initial concise vector without anything inside**/
    public conciseVector(int nD, List<Integer> iD) {
		numDemes=nD;
		commonValue=0.0;
		diffIndeces = new ArrayList<Integer>();
		diffValues = new ArrayList<Double>();
		if (iD!=null) inactiveDemes=new ArrayList<Integer>(iD);
		else inactiveDemes=new ArrayList<Integer>();
	}
    
    /**Create (not really) concise vector being just a list of doubles**/
    public conciseVector(ArrayList<Double> lP) {
		numDemes=lP.size();
		commonValue=0.0;
		diffIndeces = new ArrayList<Integer>();
		diffValues = new ArrayList<Double>();
		for (int i=0;i<lP.size();i++) diffValues.add(lP.get(i));
		inactiveDemes=new ArrayList<Integer>();
	}

    /**join two ordered lists of indeces**/
    public List<Integer> joinIndeces(List<Integer> l1, List<Integer> l2){
    	//System.out.println("joining lists: 1 "+l1+" 2 "+l2);
    	if (l1.size()==0) return l2;
    	if (l2.size()==0) return l1;
    	List<Integer> l3 = new ArrayList<Integer>();
    	int i1=0;
    	int i2=0;
    	int e1=l1.get(i1);
    	int e2=l2.get(i2);
    	int s1=l1.size();
    	int s2=l2.size();
    	while (i1< s1 && i2< s2 ){
    		if (e1<e2){
    			l3.add(e1);
    			i1++;
    			if (i1<s1) e1=l1.get(i1);
    		}else if (e2<e1){
    			l3.add(e2);
    			i2++;
    			if (i2<s2) e2=l2.get(i2);
    		}else{
    			l3.add(e2);
    			i2++;
    			i1++;
    			if (i2<s2) e2=l2.get(i2);
    			if (i1<s1) e1=l1.get(i1);
    		}
    	}
    	for (;i1<s1;i1++) l3.add(l1.get(i1));
    	for (;i2<s2;i2++) l3.add(l2.get(i2));
    	
    	return l3;
    }
    
    /** Add Integer to sorted list **/
    public List<Integer> addSorted(List<Integer> list, Integer e){
    	int i=0;
    	while (i<list.size()) {
    		if (list.get(i)>=e){
    			list.add(i,e);
    			return list;
    		}
    		i++;
    	}
    	list.add(e);
    	return list;
    }
    
    /** Remove Integer from sorted list **/
    public List<Integer> removeElement(List<Integer> list, Integer e){
    	int i=0;
    	while (i<list.size()) {
    		if ((list.get(i)).equals(e)){
    			list.remove((Integer)e);
    			return list;
    		}
    		i++;
    	}
    	return list;
    }
    
    /** Add deme backward in time **/
    public void addDeme(int type){
    	inactiveDemes=removeElement(inactiveDemes, type);
    	int i=0;
    	for (;i<diffIndeces.size();i++){
    		if (diffIndeces.get(i)>type)
    			break;
    		if (diffIndeces.get(i)==type) {
    			System.out.println("Error, the deme to be added was already in!");
    			System.exit(0);
    		}
    	}
    	diffIndeces.add(i,type);
    	diffValues.add(i,0.0);
    }
    
    /** Remove deme backward in time **/
    public void removeDeme(int type){
    	inactiveDemes=addSorted(inactiveDemes, (Integer) type);
    	int i=0;
    	boolean found=false;
    	for (;i<diffIndeces.size();i++){
    		if (diffIndeces.get(i)>type)
    			break;
    		if (diffIndeces.get(i)==type) {
    			found=true;
    			diffIndeces.remove(i);
    			double value=diffValues.get(i);
    			diffValues.remove(i);
    			value=value/(numDemes-inactiveDemes.size());
    			commonValue+=value;
    			for (int j=0;j<diffValues.size();j++) diffValues.set(j, diffValues.get(j)+value);
    			break;
    		}
    	}
    	if (!found) {
    		double value=commonValue/(numDemes-inactiveDemes.size());
    		commonValue+=value;
    		for (int j=0;j<diffValues.size();j++) diffValues.set(j, diffValues.get(j)+value);
    		//System.out.println("Error, the deme to be removed was not in the list of active demes!");
			//System.exit(0);
    	}
    }
    
    
    /**Make the Hadamard product with a concise vector**/
    public conciseVector entrywiseProd(conciseVector v2){
    	int d1=numDemes;
    	int d2=v2.numDemes;
    	if (d1!=d2) {
    		System.out.println(" number of demes for the two conciseVectors are different");
    		System.exit(0);
    	}
    	for (int i=0;i<inactiveDemes.size();i++){
    		if (!((inactiveDemes.get(i)).equals(v2.inactiveDemes.get(i)))) {
    			System.out.println("Inactive demes for the two conciseVectors to be multiplied are different");
        		System.exit(0);
    		}
    	}
    	
    	conciseVector v3= new conciseVector(d1, inactiveDemes);
    	List<Integer> l1=diffIndeces;
    	List<Integer> l2=v2.diffIndeces;
    	List<Integer> l3 = joinIndeces(l1,l2);
    	v3.diffIndeces = l3;
    	
    	v3.commonValue= commonValue * v2.commonValue;
    	
    	List<Double> lV1 = diffValues;
    	List<Double> lV2 = v2.diffValues;
    	List<Double> lV3 = new ArrayList<Double>();
    	int s3=l3.size(), s2=l2.size(), s1=l1.size();
    	int i1=0, i2=0, i3=0;
    	
    	int iv3, iv1, iv2;
    	if (s3>0) iv3=l3.get(i3); else iv3=Integer.MIN_VALUE;
    	if (s2>0) iv2=l2.get(i2); else iv2=Integer.MIN_VALUE;
    	if (s1>0) iv1=l1.get(i1); else iv1=Integer.MIN_VALUE;
    	
    	for (i3=0; i3< s3;i3++){
    		if (i3<s3) iv3=l3.get(i3);
			if (i2<s2) iv2=l2.get(i2);
			if (i1<s1) iv1=l1.get(i1);
    		if (iv3==iv1 && iv3==iv2){
    			lV3.add(lV1.get(i1)*lV2.get(i2));
    			i1++;
    			i2++;
    		}else if (iv3==iv1){
    			lV3.add(lV1.get(i1)*v2.commonValue);
    			i1++;
    		}else if (iv3==iv2){
    			lV3.add(lV2.get(i2)*commonValue);
    			i2++;
    		}
    	}
    	v3.diffValues = lV3;
    	
    	return v3;
    }
    
    
    
    /**return concise vector with squared entries**/
    public conciseVector square(){
    	conciseVector v3= new conciseVector(numDemes, inactiveDemes);
    	v3.diffIndeces = new ArrayList<Integer>(diffIndeces);
    	
    	v3.commonValue= commonValue * commonValue;
    	
    	List<Double> lV3 = new ArrayList<Double>();
    	int s1=diffIndeces.size();
    	for (int i1=0; i1< s1;i1++){
    		lV3.add(diffValues.get(i1)*diffValues.get(i1));
    	}
    	v3.diffValues=lV3;
    	
    	return v3;
    }
    
    
    /** entry-wise sum a conciseVector to an existing one
     * @return **/
    public void sum(conciseVector v){
    	int d1=numDemes;
    	int d2=v.numDemes;
    	if (d1!=d2) {
    		System.out.println(" number of demes for the two conciseVectors are different");
    		System.exit(0);
    	}
    	for (int i=0;i<inactiveDemes.size();i++){
    		if (inactiveDemes.get(i).intValue()!=v.inactiveDemes.get(i).intValue()) {
    			System.out.println("Inactive demes for the two conciseVectors to be added are different");
        		System.exit(0);
    		}
    	}
    	
    	//conciseVector v3= new conciseVector(d1);
    	List<Integer> l1=v.diffIndeces;
    	List<Integer> l2=diffIndeces;
    	//List<Integer> l2=v2.diffIndeces;
    	List<Integer> l3 = joinIndeces(l1,l2);
    	//diffIndeces = l3;
    	
    	
    	
    	List<Double> lV1 = v.diffValues;
    	List<Double> lV2 = diffValues;
    	List<Double> lV3 = new ArrayList<Double>();
    	int s1=lV1.size();
    	int s2=diffValues.size();
    	int s3=l3.size();
    	int i1=0, i2=0, i3=0, iv1=-1, iv2=-1, iv3=-1;
    	for (i3=0; i3< s3;i3++){
    		if (i3<s3) iv3=l3.get(i3);
    		if (i2<s2) iv2=l2.get(i2);
    		if (i1<s1) iv1=l1.get(i1);
    		if (iv3==iv1 && iv3==iv2){
    			lV3.add(lV1.get(i1) + lV2.get(i2));
    			i1++;
    			i2++;
    		}else if (iv3==iv1){
    			lV3.add(lV1.get(i1) + commonValue);
    			i1++;
    		}else if (iv3==iv2){
    			lV3.add(lV2.get(i2) + v.commonValue);
    			i2++;
    		}
    	}
    	//v3.diffValues = lV3;
    	
    	commonValue+= v.commonValue;
    	diffIndeces=l3;
    	diffValues=lV3;	
    	//return v3;
    }
    
    
    
    /**find sum of all entries in conciseVector**/
    public double sumV(){
    	double sum=0;
    	sum+=commonValue*(numDemes - (diffIndeces.size() + inactiveDemes.size()));
    	for (int i=0;i<diffValues.size();i++){
    		sum+=diffValues.get(i);
    	}
    	return sum;
    }
    
    
    /**Sample an element at random from a conciseVector proportionally to its value**/
    public int sample(){
    	double x = Randomizer.nextDouble();
    	double sum=this.sumV();
    	x=x*sum;
    	sum=0.0;
    	int type=-1;
    	for (int i = 0; i<diffValues.size(); i++) {
    		sum+=diffValues.get(i);
    		if (x<sum){
    			type=diffIndeces.get(i);
    			break;
    		}
    	}
    	if (x>sum){
    		type=(int) Math.floor((x-sum)/commonValue);
    		List<Integer> newList = joinIndeces(diffIndeces, inactiveDemes);
    		for (int i = 0; i<newList.size(); i++){
    			if (newList.get(i)<=type){
    				type++;
    			}else break;
    		}
    	}
    	return type;
    }
    
    
    /**scale a concise vectors by a real number**/
    public void scaleV(double scale){
    	commonValue=commonValue*scale;
    	int num1=diffIndeces.size();
    	for (int i=0;i<num1;i++){
    		diffValues.set(i,diffValues.get(i)*scale);
    	}
    }
    
    /**add some value to one of the entries**/
    public void add(int deme, double value){
    	boolean found=false;
    	int i=0;
    	for (;i<diffIndeces.size();i++){
    		if (diffIndeces.get(i)>deme)
    			break;
    		if (diffIndeces.get(i)==deme) {
    			found=true;
    			diffValues.set(i, diffValues.get(i)+value);
    			break;
    		}
    	}
    	if (!found) {
    		diffIndeces.add(i,deme);
    		diffValues.add(i,commonValue+value);
    	}
    	if(inactiveDemes.contains(deme)) {
    		System.out.println("Deme "+deme+" is inactive, cannot add value to it. Try to set Epi deme closure dates slightly after sampling date in same deme.");
    		System.exit(0);
    	}
    }
    
    
    /**multiply a concise vectors by matrix exponential. Lambda is time * migration rate * number of demes    **/
    public conciseVector matrixMul(conciseVector v1, double lambda){
    	int numD=v1.numDemes;
    	/**entries of the prob matrix**/
    	double activeDemes=numD-inactiveDemes.size();
    	double stay=(1.0/activeDemes) + ((activeDemes-1.0)/activeDemes)*Math.exp(-lambda);
    	double leave=(1.0/activeDemes) - ((1.0)/activeDemes)*Math.exp(-lambda);
    	
    	int d1=v1.numDemes;
    	conciseVector v2= new conciseVector(d1, inactiveDemes);
    	List<Integer> l1;
    	if (v1.diffIndeces!=null) l1= new ArrayList<Integer>(v1.diffIndeces);
    	else l1= new ArrayList<Integer>();
    	//List<Integer> l1= new ArrayList<Integer>(v1.diffIndeces);
    	v2.diffIndeces = l1;
    	
    	double cV=0.0;
    	List<Double> lV1=v1.diffValues;
    	int num1=l1.size();
    	for (int i=0;i<num1;i++){
    		cV+= lV1.get(i)*leave;
    	}
    	double allLeave=cV;
    	/**allLeave contains the total sum of outgoing probability- made to save computational time**/
    	allLeave+=(activeDemes-l1.size())*leave*v1.commonValue;
    	if (activeDemes>l1.size()+1){
    		cV+=(activeDemes-(l1.size()+1))*leave*v1.commonValue;
    	}
    	cV+=v1.commonValue*stay;
    	v2.commonValue=cV;
    	
    	List<Double> lV2 = new ArrayList<Double>();
    	for (int i=0;i<num1;i++){
    		lV2.add(allLeave+lV1.get(i)*(stay-leave));
    	}
    	v2.diffValues=lV2;
    	return v2;
    }
    
    public conciseVector matrixMul(double lambda){
    	return matrixMul(this , lambda);
    }
    
    
    /** Update likelihood to condition upon the parent deme **/
    public conciseVector conditional(double lambda, int parentDeme){
    	int numD=this.numDemes;
    	double activeDemes=numD-this.inactiveDemes.size();
    	/**entries of the prob matrix**/
    	double stay=(1.0/activeDemes) + ((activeDemes-1.0)/activeDemes)*Math.exp(-lambda);
    	double leave=(1.0/activeDemes) - ((1.0)/activeDemes)*Math.exp(-lambda);
    	
    	int d1=this.numDemes;
    	conciseVector v2= new conciseVector(d1, this.inactiveDemes);
    	List<Integer> l1;
    	if (this.diffIndeces!=null) l1= new ArrayList<Integer>(this.diffIndeces);
    	else l1= new ArrayList<Integer>();
    	List<Double> lV1=this.diffValues;
    	List<Double> lV2 = new ArrayList<Double>();
    	boolean found=false;
    	int newIndex=-1;
    	for (int i=0; i<l1.size(); i++){
    		if ((l1.get(i)).equals(parentDeme)){
    			lV2.add(lV1.get(i)*stay);
    			found=true;
    		}else{
    			if (l1.get(i)>parentDeme && newIndex==-1) newIndex=i;
    			lV2.add(lV1.get(i)*leave);
    		}
    	}
    	if (found==false) {
    		if (l1.size()==0 || newIndex==-1){
    			l1.add(parentDeme);
        		lV2.add(this.commonValue*stay);
    		}else{
    			l1.add(newIndex, parentDeme);
        		lV2.add(newIndex, this.commonValue*stay);
    		}
    	}
    	
    	v2.diffIndeces = l1;
    	v2.diffValues=lV2;
    	v2.commonValue=this.commonValue*leave;
    	return v2;
    }
    
    
	public Double get(int type) {
		for (int i=0; i<diffIndeces.size(); i++){
			if (type==diffIndeces.get(i)) return this.diffValues.get(i);
		}
		return this.commonValue;
	}
	
	


    //debugging
    public static void main(String[] argv) throws Exception {
    	int n=10;
    	List<Integer> iD= new ArrayList<Integer>();
    	iD.add(7);
    	iD.add(8);
    	conciseVector v1 = new conciseVector(n,iD);
    	v1.add(1, 1.0);
    	v1.add(2, 2.0);
    	v1.add(3, 3.0);
    	v1.add(1, -1.0);
    	v1.add(4, -1.0);
    	System.out.println("Sum: "+v1.sumV());
    	System.out.println("v1: "+v1.numDemes+" "+v1.diffIndeces+" "+v1.diffValues+" "+v1.commonValue+" ");
    	conciseVector v2 = new conciseVector(n,3,iD);
    	v2.add(2, 2.0);
    	conciseVector v4=v2.copy();
    	v4.commonValue=0.1;
    	v4.add(6, 2.5);
    	conciseVector v3 = v2.copy();
    	v3.add(6, 2.5);
    	System.out.println("v2: "+v2.numDemes+" "+v2.diffIndeces+" "+v2.diffValues+" "+v2.commonValue+" ");
    	System.out.println("v3: "+v3.numDemes+" "+v3.diffIndeces+" "+v3.diffValues+" "+v3.commonValue+" ");
    	v3.scaleV(2.0);
    	System.out.println("v3: "+v3.numDemes+" "+v3.diffIndeces+" "+v3.diffValues+" "+v3.commonValue+" "+v3.sumV());
    	v3.commonValue=0.1;
    	v3.sum(v1);
    	System.out.println("v3: "+v3.numDemes+" "+v3.diffIndeces+" "+v3.diffValues+" "+v3.commonValue+" ");
    	v2=v3.square();
    	System.out.println("v3 squared: "+v2.numDemes+" "+v2.diffIndeces+" "+v2.diffValues+" "+v2.commonValue+" ");
    	v2=v4.entrywiseProd(v1);
    	System.out.println("v4 entrywise product v1: "+v2.numDemes+" "+v2.diffIndeces+" "+v2.diffValues+" "+v2.commonValue+" ");
    	v2=v3.matrixMul(v3, 0.01);
    	iD=v2.addSorted(iD,9);
    	iD=v2.addSorted(iD,6);
    	iD=v2.addSorted(iD,4);
    	iD=v2.addSorted(iD,1);
    	v1 = new conciseVector(n,iD);
    	v1.add(2,0.3);
    	v1.add(3,0.5);
    	v1.add(5,0.2);
    	//v1.commonValue=0.1;
    	System.out.println("v1: "+v1.numDemes+" "+v1.diffIndeces+" "+v1.diffValues+" "+v1.commonValue+" ");
    	v2=v1.matrixMul(1.0);
    	System.out.println("v1 matrix mult : "+v2.numDemes+" "+v2.diffIndeces+" "+v2.diffValues+" "+v2.commonValue+" "+v2.inactiveDemes+" ");
    	v2.addDeme(1);
    	System.out.println("v2 added deme : "+v2.numDemes+" "+v2.diffIndeces+" "+v2.diffValues+" "+v2.commonValue+" "+v2.inactiveDemes+" ");
    	v2.removeDeme(2);
    	System.out.println("v2 removed deme : "+v2.numDemes+" "+v2.diffIndeces+" "+v2.diffValues+" "+v2.commonValue+" "+v2.inactiveDemes+" ");
    }

}
