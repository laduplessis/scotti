/*
 * Copyright (C) 2015 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.evolution.tree;


import beast.core.Description;

import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Arrays;
import java.util.List;

//import com.google.common.collect.Lists;

/**
 *
 * @author Nicola De Maio
 */
@Description("A concise representation of node lists each referring to a deme.")
public class conciseVectorNodeList {
	
	public int numDemes;
	/**value shared by most entries**/
	//public double commonValue;
	/**list of populations that have uncommon value**/
	public List<Integer> diffIndeces;
	/**List of unusual values**/
	public List<List<MultiTypeNodeConcise>> diffValues;
	
	/**Create inital concise vector without anything inside**/
    public conciseVectorNodeList(int nD) {
		numDemes=nD;
		//commonValue=0.0;
		diffIndeces = new ArrayList<Integer>();
		diffValues = new ArrayList<List<MultiTypeNodeConcise>>();
	}


    
    /**add a node to one of the demes**/
    public void add(int deme, MultiTypeNodeConcise value){
    	boolean found=false;
    	int i=0;
    	for (;i<diffIndeces.size();i++){
    		if (diffIndeces.get(i)>deme)
    			break;
    		if (diffIndeces.get(i)==deme) {
    			found=true;
    			diffValues.get(i).add(value);
    			break;
    		}
    	}
    	if (!found) {
    		diffIndeces.add(i,deme);
    		List<MultiTypeNodeConcise> listN = new ArrayList<MultiTypeNodeConcise>();
    		listN.add(value);
    		diffValues.add(i, listN);
    	}
    }
    
    
    /**total number of nodes**/
    public int sum(){
    	int sum=0;
    	for (int i=0;i<diffIndeces.size();i++){
    		sum+=diffValues.get(i).size();
    	}
    	return sum;
    }
    
    
    /**remove first value to one of the entries**/
    public void remove(int deme){
    	//MultiTypeNodeVolz node = null;
    	int i=0;
    	for (;i<diffIndeces.size();i++){
    		if (diffIndeces.get(i)>deme)
    			break;
    		if (diffIndeces.get(i)==deme) {
    			//node = diffValues.get(i).get(0);
    			diffValues.get(i).remove(0);
    			if (diffValues.get(i).isEmpty()){
    		    	diffValues.remove(i);
    		    	diffIndeces.remove(i);
    		    }
    			break;
    		}
    	}
    	//return node;
    }
    
    



}
