/*
 * Copyright (C) 2015 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.evolution.tree;

import beast.core.CalculationNode;
import beast.core.Description;
import beast.core.Input;
import beast.core.Input.Validate;
import beast.core.Loggable;
//import beast.core.parameter.BooleanParameter;
import beast.core.parameter.RealParameter;
import beast.core.parameter.IntegerParameter;
//import beast.evolution.substitutionmodel.*;






import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import multitypetreeVolz.util.InfectionTraitSet;

import org.jblas.DoubleMatrix;
//import org.jblas.ComplexDoubleMatrix;
import org.jblas.MatrixFunctions;
//import org.jblas.Eigen;


/**
 * @author Nicola De Maio
 */
@Description("Basic plugin describing a uniform Markovian migration model.")
public class MigrationModelUniform extends CalculationNode implements Loggable {

    public Input<RealParameter> rateInput = new Input<RealParameter>(
            "rate",
            "Migration rate",
            Validate.REQUIRED);
    public Input<RealParameter> popSizeInput = new Input<RealParameter>(
            "popSize",
            "Demes population size.",
            Validate.REQUIRED);
    public Input<IntegerParameter> numDemesInput = new Input<IntegerParameter>(
            "numDemes",
            "Number of demes",
            Validate.REQUIRED);
    public Input<Integer> minDemesInput = new Input<Integer>(
            "minDemes",
            "Minimum number of demes",
            Validate.REQUIRED);
    public Input<List<InfectionTraitSet>> m_traitList = new Input<List<InfectionTraitSet>>("trait",
            "trait information for initializing traits, in particular epi data for hosts",
            new ArrayList<InfectionTraitSet>());
//    public Input<BooleanParameter> rateMatrixFlagsInput = new Input<BooleanParameter>(
//            "rateMatrixFlags",
//            "Optional boolean parameter specifying which rates to use."
//            + " (Default is to use all rates.)");
    
    public Map<String, Double> startValues;
    public Map<String, Double> endValues;
    
    private RealParameter rate, popSize;
    private int nTypes;
    private double m;
    //private double pS;
    //private BooleanParameter rateMatrixFlags;
    //private double totalPopSize;
    //private double mu, muSym;
    private IntegerParameter numDemes;
    private Integer minDemes;
    private double R;
    //private DoubleMatrix Qsym, Rsym;
    private List<Double> RpowNStay, RpowNLeave;// RsymPowN;
    //private double RpowMaxStay, RpowMaxLeave;// RsymPowMax;
    private boolean RpowSteady;// RsymPowSteady;
    //private boolean useJblas=false;
    
    //private boolean rateMatrixIsSquare, symmetricRateMatrix;
    
    // Flag to indicate whether EV decompositions need updating.
    private boolean dirty;
    //private double[][] MigInstRateMatrix;
    //private double[] MigEigenvalues, MigEigens, MigIEigens;
    //private ComplexDoubleMatrix MigEigenvalues2, MigEigens2, MigIEigens2;

    public MigrationModelUniform() { }

    @Override
    public void initAndValidate() throws IllegalArgumentException {
        
        popSize = popSizeInput.get();
        numDemes = numDemesInput.get();
        rate = rateInput.get();
        minDemes = minDemesInput.get();
        //nTypes=numDemes.getValue();
        
//        if (rateMatrixFlagsInput.get() != null)
//            rateMatrixFlags = rateMatrixFlagsInput.get();
        
        rate.setLower(0.0);
        popSize.setLower(0.0);
        numDemes.setLower(minDemes);
        
        // Initialise caching array for powers of uniformized
        // transition matrix:
        RpowNStay = new ArrayList<Double>();
        RpowNLeave = new ArrayList<Double>();
        //RsymPowN = new ArrayList<DoubleMatrix>();
        
        startValues= new HashMap<String, Double>();
        endValues= new HashMap<String, Double>();
        
        processTraits(m_traitList.get());
        
        dirty = true;
        updateMatrices();
    }
    
    /**
     * Process trait sets.
     *
     * @param traitList List of trait sets.
     */
    protected void processTraits(List<InfectionTraitSet> traitList) {
    	//System.out.println("Process infction traits ");
        for (InfectionTraitSet traitSet : traitList) {
        	//System.out.println("New infection trait: ");
        	Map<String, Double> values=traitSet.getValues();
        	//System.out.println("Got values ");
        	//System.out.println("Infection trait type:  "+traitSet.type);
        	if (traitSet.type.equals("start"))
        		startValues.putAll(values);
        	if (traitSet.type.equals("end"))
        		endValues.putAll(values);
        	//System.out.println("Done infection trait ");
//            for (Node node : getExternalNodes())
//                node.setMetaData(traitSet.getTraitName(), traitSet.getValue(node.getNr()));
//            if (traitSet.isDateTrait())
//                timeTraitSet = traitSet;
        }
        //traitsProcessed = true;
    }
    
    /**
     * Ensure all local fields including matrices and eigenvalue decomposition
     * objects are consistent with current values held by inputs.
     */
    public void updateMatrices()  {
        
        if (!dirty)
            return;
        
        popSize = popSizeInput.get();
        rate = rateInput.get();
        numDemes =numDemesInput.get();
        minDemes = minDemesInput.get();
        m=rate.getValue();
        //pS=popSize.getValue();

        // Set up uniformized backward transition rate matrices R and Rsym:
        nTypes=numDemes.getValue();
        R=1.0/(nTypes-1.0);
        //R = Q.mul(1.0/mu).add(DoubleMatrix.eye(nTypes));
        //Rsym = Qsym.mul(1.0/muSym).add(DoubleMatrix.eye(nTypes));
        
        // Clear cached powers of R and steady state flag:
        RpowNStay.clear();
        RpowNLeave.clear();
        
        RpowSteady = false;
        //RsymPowSteady = false;
        
        //Power sequences initially contain R^0 = I   /////////////////////add R^0????????????????????????????????????????????????????????????????
        RpowNStay.add(1.0);
        RpowNStay.add(0.0);
        RpowNStay.add(1.0/(nTypes-1.0));
        RpowNLeave.add(0.0);
        RpowNLeave.add(1.0/(nTypes-1.0));
        RpowNLeave.add((nTypes-2.0)/(nTypes-1.0));
        
        //RpowMaxStay = 1.0;
        //RpowMaxLeave = 0.0;

        dirty = false;
    }

//    /**
//     * @return number of demes in the migration model.
//     */
//    public int getNDemes() {
//        return nTypes;
//    }
    
    /**
     * @return Probability of staying in the same deme after time distance.
     * @param distance: branch length 
     */
    public double getStay(double distance) {
    	return (1.0/nTypes) +((nTypes-1.0)*Math.exp(-m*distance*nTypes))/nTypes ;
    }
    
    /**
     * @return Probability of staying in the same deme after time distance.
     * @param distance: branch length 
     */
    public double getLeave(double distance) {
    	return (1.0/nTypes) -(Math.exp(-m*distance*nTypes)/nTypes) ;
    }

    /**
     * Obtain migration rate
     */
    public double getRate() {
    	return m;
    }
    

    /**
     * Set migration rate.
     * This method should only be called by operators.
     */
    public void setRate(double r) {
        rate.setValue(r);
        dirty = true;
    }
    
    /**
     * Set number of demes.
     * This method should only be called by operators.
     */
    public void setNumDemes(int n) {
        numDemes.setValue(n);
        dirty = true;
    }
    
    /**
     * Get number of demes.
     */
    public int getNumDemes() {
    	return numDemes.getValue();
    }
    
    /**
     * Get number of demes.
     */
    public int getMinNumDemes() {
    	return minDemes;
    }
    
    /**
     * Obtain effective population size.
     * @return Effective population size.
     */
    public double getPopSize() {
        return popSize.getValue();
    }
    
    /**
     * Set effective population size.
     */
    public void setPopSize(double newSize) {
        popSize.setValue(newSize);
        dirty = true;
    }
    
    public double getR() {
        updateMatrices();
        return R;
    }
    
    /**
     * Return diagonal element of R^n
     */
    public Double getRpowNStay(int n) {
        updateMatrices();
        if (n>=RpowNStay.size()) {   
            // Steady state of matrix iteration already reached
            if (RpowSteady) return RpowNStay.get(RpowNStay.size()-1);   
            int startN = RpowNStay.size();
            for (int i=startN; i<=n; i++) {
            	RpowNStay.add(RpowNLeave.get(i-1));
                RpowNLeave.add(RpowNLeave.get(i-2)/(nTypes-1.0) + (nTypes-2.0)*RpowNLeave.get(i-1)/(nTypes-1.0));
                //stayPowerMax.maxi(powerListS.get(i));    
                // Occasionally check whether matrix iteration has reached steady state
                if (i%10 == 0) {
                    double diff=Math.abs(RpowNLeave.get(i-1)-RpowNLeave.get(i));
                    if (!(diff>0)) {
                        RpowSteady = true;
                        return RpowNStay.get(i);
                    }
                }
            }
        }
        return RpowNStay.get(n);
    }
    
    /**
     * Return off-diagonal element of R^n
     */
    public Double getRpowNLeave(int n) {
        updateMatrices();
        if (n>=RpowNLeave.size()) {
        	if (RpowSteady) return RpowNLeave.get(RpowNLeave.size()-1);  
        	getRpowNStay(n); 
        }
        return RpowNLeave.get(n);
    }
    
    /**
     * Return matrix containing upper bounds on elements from the powers
     * of R if known.  Returns a matrix of ones if steady state has not yet
     * been reached.
     * 
     * @param symmetric
     * @return Matrix of upper bounds.
     */
    public double getRpowMax() {
        if (RpowSteady) return Math.max(RpowNLeave.get(RpowNLeave.size()), RpowNStay.get(RpowNStay.size()));
        else return 1.0;
    }
    
    
    /**
     * Power above which R is known to be steady.
     * 
     * @param symmetric
     * @return index of first known steady element.
     */
    public int RpowSteadyN() {
        if (RpowSteady)
            return RpowNLeave.size();
        else
            return -1;
    }

    /*
     * CalculationNode implementations.
     */
    
    @Override
    protected boolean requiresRecalculation() {
        // we only get here if something is dirty
        dirty = true;
        return true;
    }

    @Override
    protected void restore() {
        dirty = true;
        super.restore();
    }

    /*
     * Methods implementing loggable interface
     */
    
    @Override
    public void init(PrintStream out) throws IllegalArgumentException {
        
        String outName;
        if (getID() == null || getID().matches("\\s*"))
            outName = "migModel";
        else
            outName = getID();
        
        out.print(outName + ".popSize\t");
//        for (int i=0; i<nTypes; i++) {
//            out.print(outName + ".popSize_" + i + "\t");
//        }
        out.format("%s.rateMig\t", outName);
//        for (int i=0; i<nTypes; i++) {
//            for (int j=0; j<nTypes; j++) {
//                if (i==j)
//                    continue;
//                out.format("%s.rateMatrix_%d_%d\t", outName, i, j);
//            }
//        }
        out.print(outName + ".numPops\t");
      
    }

    @Override
    public void log(int nSample, PrintStream out) {
        
    	out.print(getPopSize() + "\t");
//        for (int i=0; i<nTypes; i++) {
//            out.print(getPopSize(i) + "\t");
//        }
    	
    	out.format("%g\t", m);
//        for (int i=0; i<nTypes; i++) {
//            for (int j=0; j<nTypes; j++) {
//                if (i==j)
//                    continue;
//                out.format("%g\t", getRateForLog(i, j));
//            }
//        }
    	out.print(getNumDemes() + "\t");

    }

    @Override
    public void close(PrintStream out) {
    }
    
    /**
     * Main for debugging.
     *
     * @param args
     */
    public static void main (String [] args) {
        
        int n=10;
        DoubleMatrix Q = new DoubleMatrix(n, n);
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                Q.put(i, j, i*n+j);
            }
        }
        MatrixFunctions.expm(Q.mul(0.001)).print();
        Q.print();
        
    }
}
